��    R      �  m   <      �     �               "     ;     C     P     d     ~     �     �     �  "   �     �                1     H     `     r     �     �     �     �      �  $   	  .   *	     Y	     n	     v	     |	     �	     �	     �	     �	     �	     �	     
  
   
      
     ;
     D
     V
     p
     �
     �
     �
     �
     �
     �
     �
               !     ;     S     k     z     �     �     �     �     �     �               -     ;     O     e     w     �     �     �     �     �     �  
   �     �            �  $            
   .     9     N     V     n     �     �     �     �     �  )   �     !     7     R  !   h     �     �     �     �      �     �          0  "   O  3   r     �     �     �     �     �     �  !   	      +     L  #   k     �     �  !   �     �     �          !     6     ;     Z     x     �     �     �     �     �  "   �     
     *     A     U     h     }     �  !   �     �     �     �               4     P     l     �     �     �     �     �     �       
        !     9  
   I            F           ,              (   7         6   E              M       2   "   N                 Q       H                     O   $       *      J      <   >                       R   C      D   3   L          8   0   9   ?          
   P   '                 A   G      -       4      I   &      !   1   ;   .          +   )             =   #   K   :   	   @               /   B          5   %        Disconnectshares 1-to-1_assistance 1-to-1_voice ADSL/PPPOE configuration Adblock Adjust Mixer Alsamixer Equalizer Alternatives Configurator Backlight brightness Backup Your System Boot Repair Change Gtk2 and Icon Themes Change Keyboard Layout for Session Change Slim Background Choose Startup Services Choose Wallpaper Configure Automounting Configure Connectshares Configure Dial-Up Configure GPRS/UMTS Configure Mouse Configure live persistence Configure wpa_supplicant Connect Wirelessly (wicd) Create Live-USB (live-usb-maker) Create Live-USB (live-usb-maker-gui) Create Live-USB retain partitions (UNetbootin) Create Snapshot(ISO) Desktop Disks Drivers Edit Bootloader menu Edit Config Files Edit Exclude files Edit Fluxbox Settings Edit IceWM Settings Edit System Monitor(conky) Edit jwm Settings Edit menus Grub Boot Image (jpg only) Hardware Image a Partition Install Restricted Codecs Install antiX Linux Live Live-usb kernel updater MS Windows Wireless Drivers Maintenance Manage APT Repositories Manage Firewall Manage Packages Mount Connected Devices Network Network Interfaces (ceni) Network Troubleshooting Nvidia Driver Installer PC Information Package Installer Partition a Drive Password Prompt(su/sudo) Preferred Applications Remaster-Customize Live Save root persistence Session Set Date and Time Set Default Sound Card Set Font Size Set Screen Blanking Set Screen Resolution Set System Keymap Set auto-login Set up live persistence Setup a Printer Share Files via Droopy Shares Synchronize Directories System Test Sound User Desktop-Session User Manager ssh-conduit Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2018-02-28 18:46+0200
PO-Revision-Date: 2019-01-08 18:36+0200
Last-Translator: Pierluigi Mario <pierluigimariomail@gmail.com>
Language-Team: Italian (http://www.transifex.com/anticapitalista/antix-development/language/it/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: it
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 1.8.11
 Disconnetti le condivisioni Assistenza 1 a 1 Voce 1 a 1 Configura ADSL/PPPOE Adblock Alsa-Mixer Canali Audio Alsa-Mixer Equalizzatore Configura alternative Retroilluminazione schermo Backup del sistema Ripristina boot Cambia temi di Gtk2 e icone Cambia mappatura tastiera per la Sessione Cambia sfondo di Slim Scegli i servizi all'avvio Cambia sfondo desktop Configura il montaggio automatico Configura le condivisioni Configura Dial-Up Configura GPRS/UMTS Configura il mouse Configura persistenza della Live Configura wpa_supplicant Connetti wireless (wicd) Crea Live USB (live-usb-maker) Crea Live USB (live-usb-maker-gui) Crea Live USB mantenendo le partizioni (UNetbootin) Crea snapshot (ISO) Desktop Dischi Driver Modifica il menu di boot Modifica file di configurazione Modifica il file delle esclusioni Modifica impostazioni di Fluxbox Modifica impostazioni di IceWM Modifica monitor di sistema (conky) Modifica impostazioni di jwm Modifica i menu Immagine del boot Grub (solo jpg) Hardware Crea immagine della partizione Installa codec proprietari Installa antiX Linux Live Aggiorna kernel della Live-usb Driver wireless di MS Windows Manutenzione Gestisci repository APT Gestisci il firewall Gestore pacchetti Monta i dispositivi collegati Rete Gestisci Interfacce di Rete (ceni) Risoluzione problemi della rete Installa driver Nvidia Informazioni sul PC Installa Programmi Partiziona un'unità Richiesta password (su/sudo) Applicazioni preferite Rimasterizza/Personalizza la Live Salva persistenza root Sessione Imposta data e ora Scheda audio predefinita Dimensione Carattere Imposta sospensione schermo Imposta risoluzione schermo Imposta mappatura tastiera Imposta l'auto-login Imposta persistenza della Live Imposta stampanti Condividi file tramite Droopy Condivisioni Sincronizza directory Sistema Test audio Sessione desktop utente Gestione utenti Tunnel ssh 