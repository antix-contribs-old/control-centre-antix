��    R      �  m   <      �     �               "     ;     C     P     d     ~     �     �     �  "   �     �                1     H     `     r     �     �     �     �      �  $   	  .   *	     Y	     n	     v	     |	     �	     �	     �	     �	     �	     �	     
  
   
      
     ;
     D
     V
     p
     �
     �
     �
     �
     �
     �
     �
               !     ;     S     k     z     �     �     �     �     �     �               -     ;     O     e     w     �     �     �     �     �     �  
   �     �            �  $     �                    5     =     N     n  !   �     �     �     �  )   �     (     A     Y     m     �  !   �     �     �     �          "  "   <  &   _  <   �     �     �     �     �  "        &     <     Y     o  !   �     �     �     �     �     �          )     B     G     e     }     �     �     �     �     �     �     �          3     B     T  !   s     �     �     �     �     �  "   �          0     O  &   d     �     �     �     �  	   �     �             "   -     P     c            F           ,              (   7         6   E              M       2   "   N                 Q       H                     O   $       *      J      <   >                       R   C      D   3   L          8   0   9   ?          
   P   '                 A   G      -       4      I   &      !   1   ;   .          +   )             =   #   K   :   	   @               /   B          5   %        Disconnectshares 1-to-1_assistance 1-to-1_voice ADSL/PPPOE configuration Adblock Adjust Mixer Alsamixer Equalizer Alternatives Configurator Backlight brightness Backup Your System Boot Repair Change Gtk2 and Icon Themes Change Keyboard Layout for Session Change Slim Background Choose Startup Services Choose Wallpaper Configure Automounting Configure Connectshares Configure Dial-Up Configure GPRS/UMTS Configure Mouse Configure live persistence Configure wpa_supplicant Connect Wirelessly (wicd) Create Live-USB (live-usb-maker) Create Live-USB (live-usb-maker-gui) Create Live-USB retain partitions (UNetbootin) Create Snapshot(ISO) Desktop Disks Drivers Edit Bootloader menu Edit Config Files Edit Exclude files Edit Fluxbox Settings Edit IceWM Settings Edit System Monitor(conky) Edit jwm Settings Edit menus Grub Boot Image (jpg only) Hardware Image a Partition Install Restricted Codecs Install antiX Linux Live Live-usb kernel updater MS Windows Wireless Drivers Maintenance Manage APT Repositories Manage Firewall Manage Packages Mount Connected Devices Network Network Interfaces (ceni) Network Troubleshooting Nvidia Driver Installer PC Information Package Installer Partition a Drive Password Prompt(su/sudo) Preferred Applications Remaster-Customize Live Save root persistence Session Set Date and Time Set Default Sound Card Set Font Size Set Screen Blanking Set Screen Resolution Set System Keymap Set auto-login Set up live persistence Setup a Printer Share Files via Droopy Shares Synchronize Directories System Test Sound User Desktop-Session User Manager ssh-conduit Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2018-02-28 18:46+0200
PO-Revision-Date: 2019-01-08 18:35+0200
Last-Translator: Feh Ler <feh.ler@t-online.de>
Language-Team: German (http://www.transifex.com/anticapitalista/antix-development/language/de/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: de
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 1.8.11
 Freigaben trennen 1-zu-1_Hilfe 1-zu-1_Stimme ADSL/PPPOE-Konfiguration Adblock Toneinstellungen Alsamixer Frequenzgangkorrektur Alternativen-Konfigurator Helligkeit Hintergrundbeleuchtung Systemsicherungskopie erstellen Bootreparatur (Urladeprozedur) GTK+ und Icon Themen Tastaturbelegung für die Sitzung ändern Slim-Hintergrund ändern Startup-Dienste wählen Hintergrund wählen Automount konfigurieren Connectshares einstellen Konfigurieren der DFÜ-Verbindung GPRS-UMTS-Konfiguration Mauseinstellungen Live Persistenz konfigurieren wpa_supplicant einstellen Drahtlosverbindung (wicd) Live-USB erzeugen (live-usb-maker) Live-USB erzeugen (live-usb-maker-gui) Auf USB installieren für dauerhafte Partitionen (UNetbooin) Schnappschuss (ISO) erzeugen Oberfläche Speichermedien Treiber Menü des Bootprogramms bearbeiten Konfigurationsdateien Ausschlussdateien bearbeiten Fluxbox Einstellungen IceWM Einstellungen System-Monitor (Conky) einstellen jwm Einstellungen Menüs bearbeiten Hintergrund von GRUB (jpg) Hardware Abzug einer Partition erstellen Unfreie Codecs installieren antiX Linux installieren Live Live-usb Kernelaktualisierung MS Windows WLAN-Treiber Wartung Verwalten der APT Repos Verwalten des Firewall Verwalten der Pakete Geräte einhängen (mount) Netzwerk Netzwerkassistent Netzwerk Problemlösung Nvidia-Treiber Installation PC Information Paketinstallation Partitionieren eines Laufwerks Passwortabfrage (su/sudo) wählen Bevorzugte Anwendungen Live anpassen-remastern Speichern der Root-Persistenz Sitzung Datum & Zeit Einrichtung der Standardsoundkarte Schriftgröße setzen Bildschirmlöschung einstellen Bildschirmauflösung Systemweite Tastaturbelegung festlegen Auto-login einstellen Einrichten der Live-Persistenz Einrichten eines Druckers Dateien via Droopy teilen Freigaben Verzeichnissynchronisation System Klang testen Konfiguration der Benutzersitzung  Benutzerverwaltung ssh-conduit 