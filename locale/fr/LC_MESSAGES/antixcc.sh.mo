��    R      �  m   <      �     �               "     ;     C     P     d     ~     �     �     �  "   �     �                1     H     `     r     �     �     �     �      �  $   	  .   *	     Y	     n	     v	     |	     �	     �	     �	     �	     �	     �	     
  
   
      
     ;
     D
     V
     p
     �
     �
     �
     �
     �
     �
     �
               !     ;     S     k     z     �     �     �     �     �     �               -     ;     O     e     w     �     �     �     �     �     �  
   �     �            �  $     �          &     2     K     c     }     �     �     �     �     �  2        K     `     u  !   �  %   �     �     �     �           7     Q  $   i  ;   �  6   �               $     ,  &   4     [     u     �     �  #   �     �     �  *   �  	   '     1  "   F     i          �     �     �     �     �     �     
     &     .  "   H     k     �     �     �     �     �  '     !   +     M  $   U  !   z     �  !   �     �  -   �  &         B     c      o     �     �     �      �     �          &            F           ,              (   7         6   E              M       2   "   N                 Q       H                     O   $       *      J      <   >                       R   C      D   3   L          8   0   9   ?          
   P   '                 A   G      -       4      I   &      !   1   ;   .          +   )             =   #   K   :   	   @               /   B          5   %        Disconnectshares 1-to-1_assistance 1-to-1_voice ADSL/PPPOE configuration Adblock Adjust Mixer Alsamixer Equalizer Alternatives Configurator Backlight brightness Backup Your System Boot Repair Change Gtk2 and Icon Themes Change Keyboard Layout for Session Change Slim Background Choose Startup Services Choose Wallpaper Configure Automounting Configure Connectshares Configure Dial-Up Configure GPRS/UMTS Configure Mouse Configure live persistence Configure wpa_supplicant Connect Wirelessly (wicd) Create Live-USB (live-usb-maker) Create Live-USB (live-usb-maker-gui) Create Live-USB retain partitions (UNetbootin) Create Snapshot(ISO) Desktop Disks Drivers Edit Bootloader menu Edit Config Files Edit Exclude files Edit Fluxbox Settings Edit IceWM Settings Edit System Monitor(conky) Edit jwm Settings Edit menus Grub Boot Image (jpg only) Hardware Image a Partition Install Restricted Codecs Install antiX Linux Live Live-usb kernel updater MS Windows Wireless Drivers Maintenance Manage APT Repositories Manage Firewall Manage Packages Mount Connected Devices Network Network Interfaces (ceni) Network Troubleshooting Nvidia Driver Installer PC Information Package Installer Partition a Drive Password Prompt(su/sudo) Preferred Applications Remaster-Customize Live Save root persistence Session Set Date and Time Set Default Sound Card Set Font Size Set Screen Blanking Set Screen Resolution Set System Keymap Set auto-login Set up live persistence Setup a Printer Share Files via Droopy Shares Synchronize Directories System Test Sound User Desktop-Session User Manager ssh-conduit Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2018-02-28 18:46+0200
PO-Revision-Date: 2019-01-08 18:36+0200
Last-Translator: anticapitalista <anticapitalista@riseup.net>
Language-Team: French (http://www.transifex.com/anticapitalista/antix-development/language/fr/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: fr
Plural-Forms: nplurals=2; plural=(n > 1);
X-Generator: Poedit 1.8.11
 Déconnecter partages  Assistance 1-to-1 Voix 1-to-1 Configuration ADSL/PPPOE Bloqueur de publicités Contrôle du volume audio Égaliseur Alsamixer Configurateur d'options Luminosité du rétroéclairage Sauvegarde du système Réparation du démarrage Gestionnaire de thèmes gtk2 Modifier la disposition du clavier pour la Session Ecran d'accueil Slim Gestion des services Fond d'écran Configurer le montage automatique Configurer les partages de connection Connexion par modem RTC Configuration GPRS/UMTS Configuration de la souris Configurer la persistance 'live' Configurer wpa_supplicant Réseau sans Fil (wicd) Créer un live-USB 
(live-usb-maker) Créer un live-USB 
(interface graphique de live-usb-maker) Créer des partitions de secours live-usb (UNetbootin) Créer un instantané (ISO) Bureau Disques Pilotes Réglages du menu Chargeur d'amorçage Fichiers de configuration Editer les fichiers d'exclusion Réglages Fluxbox Réglages Icewm Editer le moniteur système (conky) Réglages jwm Modifier les menus Image d'amorçage du Grub (uniquement jpg) Matériel Copier une partition Installer des Codecs Réglementés Installer antiX Linux Live Mise à jour du noyau live-usb Pilotes sans fil MS Windows Maintenance Gérer les dépôts APT Gestion du pare-feu Gestion de paquets Montage des périphériques Réseau Interfaces Réseau (ceni) Résolution des problèmes réseau Installeur de pilote Nvidia Informations du PC Installateur des paquets Outil de partitionnement Invite mot de passe(su/sudo) Applications préférées Rematérisation/Personnalisation 'live' Enregistrer la persistance racine Session Réglage de la date et et de l'heure Définir la carte son par défaut Régler la taille de police Réglage de la veille de l'écran Taille de l'écran Définir la configuration clavier du système Configuration la connexion automatique Configurer la persistance 'live' Imprimantes Partager des fichiers via Droopy Partages Synchronisation de répertoires Outils Système Tester les périphériques audio Bureau de session utilisateur Gestionnaire des utilisateurs ssh-conduit 