��    R      �  m   <      �     �               "     ;     C     P     d     ~     �     �     �  "   �     �                1     H     `     r     �     �     �     �      �  $   	  .   *	     Y	     n	     v	     |	     �	     �	     �	     �	     �	     �	     
  
   
      
     ;
     D
     V
     p
     �
     �
     �
     �
     �
     �
     �
               !     ;     S     k     z     �     �     �     �     �     �               -     ;     O     e     w     �     �     �     �     �     �  
   �     �            �  $     �     �               -     >     K     _     z     �     �     �      �               1     =     ]     v     �     �     �     �  #   �       #   /  -   S     �  
   �     �     �     �     �     �     
     (  "   D     g     �     �     �     �     �     �               &     D     T     p     �     �     �     �     �  &   �          #     =     Q     m     �     �     �     �     �     �                 1     R     k     �     �     �     �     �     �     �     �                 F           ,              (   7         6   E              M       2   "   N                 Q       H                     O   $       *      J      <   >                       R   C      D   3   L          8   0   9   ?          
   P   '                 A   G      -       4      I   &      !   1   ;   .          +   )             =   #   K   :   	   @               /   B          5   %        Disconnectshares 1-to-1_assistance 1-to-1_voice ADSL/PPPOE configuration Adblock Adjust Mixer Alsamixer Equalizer Alternatives Configurator Backlight brightness Backup Your System Boot Repair Change Gtk2 and Icon Themes Change Keyboard Layout for Session Change Slim Background Choose Startup Services Choose Wallpaper Configure Automounting Configure Connectshares Configure Dial-Up Configure GPRS/UMTS Configure Mouse Configure live persistence Configure wpa_supplicant Connect Wirelessly (wicd) Create Live-USB (live-usb-maker) Create Live-USB (live-usb-maker-gui) Create Live-USB retain partitions (UNetbootin) Create Snapshot(ISO) Desktop Disks Drivers Edit Bootloader menu Edit Config Files Edit Exclude files Edit Fluxbox Settings Edit IceWM Settings Edit System Monitor(conky) Edit jwm Settings Edit menus Grub Boot Image (jpg only) Hardware Image a Partition Install Restricted Codecs Install antiX Linux Live Live-usb kernel updater MS Windows Wireless Drivers Maintenance Manage APT Repositories Manage Firewall Manage Packages Mount Connected Devices Network Network Interfaces (ceni) Network Troubleshooting Nvidia Driver Installer PC Information Package Installer Partition a Drive Password Prompt(su/sudo) Preferred Applications Remaster-Customize Live Save root persistence Session Set Date and Time Set Default Sound Card Set Font Size Set Screen Blanking Set Screen Resolution Set System Keymap Set auto-login Set up live persistence Setup a Printer Share Files via Droopy Shares Synchronize Directories System Test Sound User Desktop-Session User Manager ssh-conduit Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2018-02-28 18:46+0200
PO-Revision-Date: 2019-01-08 18:34+0200
Last-Translator: scootergrisen
Language-Team: Danish (http://www.transifex.com/anticapitalista/antix-development/language/da/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: da
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 1.8.11
  Disconnectshares 1-til-1_assistance 1-til-1_stemme ADSL-/PPPOE-konfiguration Reklameblokering Juster mixer Alsamixer-equalizer Alternativer-konfiguration Baggrundsbelysningens lysstyrke Sikkerhedskopiér dit system Opstartsreparation Skift Gtk2- og ikontemaer Skift tastaturlayout for session Skift Slim-baggrund Vælg opstartstjenester Vælg tapet Konfigurer automatisk montering Konfigurer connectshares Konfigurer opkaldsnetværk Konfigurer GPRS/UMTS Konfigurer mus Konfigurer live-vedvarenhed Konfigurer wpa_supplicant Opret forbindelse trådløst (wicd) Opret live-USB (live-usb-maker) Opret live-USB (live-usb-maker-gui) Opret Live-USB bevar-partitioner (UNetbootin) Opret øjebliksbillede (ISO) Skrivebord Diske Drivere Rediger opstartsindlæser-menu Rediger konfigurationsfiler Rediger ekskluder-filer Rediger Fluxbox-indstillinger Rediger IceWM-indstillinger Rediger systemovervågning (conky) Rediger jwm-indstillinger Rediger menuer Grub-opstartsbillede (kun jpg) Hardware Opret aftryk af en partition Installer begrænsede codecs Installer antiX Linux Live Live-usb kerneopdatering MS Windows trådløse drivere Vedligeholdelse Håndter APT-softwarekilder Håndter firewall Håndter pakker Monter tilsluttede enheder Netværk Netværksgrænseflader (ceni) Fejlsøgning af netværk Installationsprogram til Nvidia-driver PC-information Pakkeinstallationsprogram Partitioner et drev Adgangskodeprompt (su/sudo) Foretrukne programmer Remaster-tilpas live Gem rodvedvarenhed Session Indstil dato og klokkeslæt Indstil standardlydkort Indstil skriftstørrelse Indstil skærmblankning Indstil skærmopløsning Indstil systemets tastaturlayout Indstil automatisk login Opsæt live-vedvarenhed Opsæt en printer Del filer via Droopy Delinger Synkroniser mapper System Test lyd Brugerens skrivebordssession Brugerhåndtering ssh-conduit 