��    N      �  k   �      �     �     �     �     �     �     �          ,     ?     K  "   g     �     �     �     �     �     �               /     J     c      }  $   �     �     �     �     �     �     	     	     (	     >	     R	     m	  
   	     �	     �	     �	     �	     �	     �	     �	     
     '
     3
     K
     [
     k
     �
     �
     �
     �
     �
     �
     �
          !     8     P     f     n     �     �     �     �     �     �     �               /     6     N  
   U     `     u  V  �  ,   �  #     #   *  !   N     p  5   �  !   �  8   �  1     ,   M  J   z  5   �  )   �     %  3   =  ?   q     �     �     �  4     !   :  6   \  C   �  G   �  0        P  
   h     s  8   �  F   �  0     5   5  3   k  ;   �  1   �  #     5   1     g     �  8   �      �     �  7     2   D     w  3   �  -   �  %   �  F        Y  "   b  ,   �  ,   �     �  #   �       "   >  1   a  I   �  ;   �       -   &  H   T  .   �  0   �  !   �  :     %   Z  K   �     �  +   �       +   2     ^     m  2   �  -   �     F   !       4         )   N   K                     '   $                  C          ,      #   3              G          M          :   (   7   1                     &   -          L   5      
      B   0   *   <      .       /   H      I   @      	          =       ;                    D   "      A   +          >   8       E       J      ?          %         2       9   6                     Disconnectshares ADSL/PPPOE configuration Adblock Adjust Mixer Alsamixer Equalizer Alternatives Configurator Backlight brightness Backup Your System Boot Repair Change Gtk2 and Icon Themes Change Keyboard Layout for Session Change Slim Background Choose Startup Services Choose Wallpaper Configure Automounting Configure Connectshares Configure Dial-Up Configure GPRS/UMTS Configure Mouse Configure live persistence Configure wpa_supplicant Connect Wirelessly (wicd) Create Live-USB (live-usb-maker) Create Live-USB (live-usb-maker-gui) Create Snapshot(ISO) Desktop Disks Drivers Edit Bootloader menu Edit Config Files Edit Exclude files Edit Fluxbox Settings Edit IceWM Settings Edit System Monitor(conky) Edit jwm Settings Edit menus Grub Boot Image (jpg only) Hardware Image a Partition Install Restricted Codecs Install antiX Linux Live Live-usb kernel updater MS Windows Wireless Drivers Maintenance Manage APT Repositories Manage Firewall Manage Packages Mount Connected Devices Network Network Interfaces (ceni) Network Troubleshooting Nvidia Driver Installer PC Information Package Installer Partition a Drive Password Prompt(su/sudo) Preferred Applications Remaster-Customize Live Save root persistence Session Set Date and Time Set Default Sound Card Set Font Size Set Screen Blanking Set Screen Resolution Set System Keymap Set auto-login Set up live persistence Setup a Printer Share Files via Droopy Shares Synchronize Directories System Test Sound User Desktop-Session User Manager Project-Id-Version: antix-development
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2018-02-28 18:46+0200
PO-Revision-Date: 2019-01-08 18:38+0200
Last-Translator: Vladimir O <vldoduv@yandex.ru>
Language-Team: Russian (http://www.transifex.com/anticapitalista/antix-development/language/ru/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language: ru
Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || (n%100>=11 && n%100<=14)? 2 : 3);
X-Generator: Poedit 1.8.11
 Отключить общие ресурсы ADSL/PPPOE конфигурация Блокировка рекламы Настройка микшера Эквалайзер Alsa Альтернативный конфигуратор Яркость подсветки Резервное копирование системы Восстановление загрузчика Сменить темы gtk2 и иконок Сменить раскладку клавиатуры для сессии Изменить фон Менеджера входа Параметры автозапуска Выбрать обои Настроить автомонтирование Настройка подключения общих папок Настройка Dial-Up Настройки GPRS/UMTS Настройка мыши Настройка сохранения данных Настройка wpa_supplicant Беспроводное подключение (Wicd) Создать живой USB накопитель (live-usb-maker) Создать живой USB накопитель (live-usb-maker-gui) Создать снимок системы (ISO) Рабочий стол Диски Драйверы Редактировать меню Загрузчика Редактировать конфигурационные файлы Выбрать исключение файлов Редактировать настройки Fluxbox Редактировать настройки IceWM Настроить Системный монитор(conky) Редактировать настройки jwm Редактировать меню Фон на загрузку Grub (только Jpeg) Оборудование Образ раздела Установить несвободные кодеки Установить antiX Linux Живая сессия Средство обновления Live-usb ядра MS Windows беспроводной драйвер Поддержка Управление репозиториями APT Управление брандмауэром Управление пакетами Смонтировать подключенные устройства Сеть Настройка сети (ceni) Решение сетевых проблем Установщик Nvidia драйвера Информация о PC Установщик пакетов Создание раздела Запрос пароля(su/sudo) Предпочитаемые приложения Пересобрать-кастомизировать Живой диск Создать папку сохранения данных Сессия Установка даты и времени Установить звуковую карту по умолчанию Установить размер шрифта Установить гашение экрана Разрешение экрана Установить системную раскладку Настройка автовхода Установить сохранение данных в live-режиме Настройка печати Обмен файлами через Droopy Общие ресурсы Синхронизировать папки Система Звуковой тест Рабочий сеанс пользователя Диспетчер пользователей 